# Array Iterator (DEPRECATED)

**[std::array::IntoIter](https://doc.rust-lang.org/stable/core/array/struct.IntoIter.html) [is now stable](https://blog.rust-lang.org/2021/03/25/Rust-1.51.0.html#arrayintoiter-stabilisation), use that instead!**. I will be maintaining this crate for the foreseeable future but plan on pushing an update that produces deprecation warnings in the future.

## Migrating Off

You can easily migrate off using [comby](https://comby.dev/).

```sh
comby \
	'array_iterator::ArrayIterator::new(:[args])' \
	'std::array::IntoIter::new(:[args])' \
	.rs -in-place
```

You can also remove the dependency. This can be done automatically using [cargo-edit](https://github.com/killercup/cargo-edit#cargo-rm).

```sh
cargo rm array_iterator
cargo check
```

## Docs

[Docs](https://docs.rs/array_iterator)
